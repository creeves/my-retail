package com.target.casestudy.myretail;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({PriceRepositoryTest.class, ProductControllerTest.class, ProductServiceTest.class})
public class MyRetailTestSuite {
    public MyRetailTestSuite() {
    }
}
