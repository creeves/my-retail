package com.target.casestudy.myretail;

import com.target.casestudy.myretail.api.ProductController;
import com.target.casestudy.myretail.models.json.Item;
import com.target.casestudy.myretail.models.json.Product;
import com.target.casestudy.myretail.models.json.ProductDescription;
import com.target.casestudy.myretail.models.json.ProductJson;
import com.target.casestudy.myretail.models.response.PriceResponse;
import com.target.casestudy.myretail.models.response.ProductResponse;
import com.target.casestudy.myretail.service.ProductService;
import com.target.casestudy.myretail.models.dbmodel.Price;
import com.target.casestudy.myretail.repo.PriceRepository;
import java.util.List;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.junit.Rule;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PriceRepositoryTest {


	@Autowired
	PriceRepository priceRepository;
	private MockMvc mockMvc;

	@MockBean
	private ProductService productService;

	@Test
	public void findByValidIdReturnsPrice() {

		Optional<Price> result = priceRepository.findById(16483589);
		Price expected = new Price(16483589, 14.50, "USD");
		Assert.assertEquals(expected.toString(), result.get().toString());
	}

	@Before
	@After
	public void printBanner() {
		System.out.println("*************************************************************************************");
	}
}
