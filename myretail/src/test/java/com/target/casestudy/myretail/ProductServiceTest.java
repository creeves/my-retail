package com.target.casestudy.myretail;

import com.target.casestudy.myretail.models.dbmodel.Price;
import com.target.casestudy.myretail.models.json.Item;
import com.target.casestudy.myretail.models.json.Product;
import com.target.casestudy.myretail.models.json.ProductDescription;
import com.target.casestudy.myretail.models.json.ProductJson;
import com.target.casestudy.myretail.models.response.PriceResponse;
import com.target.casestudy.myretail.models.response.ProductResponse;
import com.target.casestudy.myretail.repo.PriceRepository;
import com.target.casestudy.myretail.service.ProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
public class ProductServiceTest {
    @Mock
    private PriceRepository priceRepository;

    @InjectMocks
    private ProductService productService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRetrieveProductInformation() throws IOException {
        ProductResponse expected = new ProductResponse(13860428, "The Big Lebowski (Blu-ray)", new PriceResponse(1.25, "USD"));
        Optional<Price> mockPrice = Optional.of(new Price(13860428, 1.25, "USD"));

        Mockito.doReturn(mockPrice).when(this.priceRepository).findById(Mockito.anyInt());

        ProductResponse result = this.productService.retrieveProductResponse(13860428);
        Assert.assertEquals(expected.getName(), result.getName());
        Assert.assertEquals(expected.getId(), result.getId());
        Assert.assertEquals(expected.getCurrent_price().getValue(), result.getCurrent_price().getValue());
        Assert.assertEquals(expected.getCurrent_price().getCurrencyCode(), result.getCurrent_price().getCurrencyCode());
    }

}