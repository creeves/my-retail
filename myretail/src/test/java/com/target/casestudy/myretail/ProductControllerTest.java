package com.target.casestudy.myretail;

        import com.target.casestudy.myretail.api.ProductController;
        import com.target.casestudy.myretail.models.dbmodel.Price;
        import com.target.casestudy.myretail.models.json.Item;
        import com.target.casestudy.myretail.models.json.Product;
        import com.target.casestudy.myretail.models.json.ProductDescription;
        import com.target.casestudy.myretail.models.json.ProductJson;
        import com.target.casestudy.myretail.models.response.PriceResponse;
        import com.target.casestudy.myretail.models.response.ProductResponse;
        import com.target.casestudy.myretail.repo.PriceRepository;
        import com.target.casestudy.myretail.service.ProductService;
        import org.junit.Assert;
        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.mockito.Mockito;
        import org.skyscreamer.jsonassert.JSONAssert;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
        import org.springframework.boot.test.context.SpringBootTest;
        import org.springframework.boot.test.mock.mockito.MockBean;
        import org.springframework.context.annotation.Import;
        import org.springframework.http.MediaType;
        import org.springframework.test.context.junit4.SpringRunner;
        import org.springframework.test.web.servlet.MockMvc;
        import org.springframework.test.web.servlet.MvcResult;
        import org.springframework.test.web.servlet.RequestBuilder;
        import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {


    @Autowired
    ProductController productController;

    public ProductControllerTest(){}

    @Test
    public void retrieveDetailsForProduct() throws Exception {

        ProductResponse result = productController.getProductInformation(13860428);

        ProductResponse expected = new ProductResponse(13860428, "The Big Lebowski (Blu-ray)", new PriceResponse(2.5, "USD"));

        Assert.assertEquals(expected.getId(), result.getId());
        Assert.assertEquals(expected.getName(), result.getName());
        Assert.assertEquals(expected.getCurrent_price().getCurrencyCode(), result.getCurrent_price().getCurrencyCode());
        Assert.assertEquals(expected.getCurrent_price().getValue(), result.getCurrent_price().getValue());
    }
}