package com.target.casestudy.myretail.repo;

import com.target.casestudy.myretail.models.dbmodel.Price;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface PriceRepository extends MongoRepository<Price, Integer> {
    @Override
    public Optional<Price> findById(Integer id);

}
