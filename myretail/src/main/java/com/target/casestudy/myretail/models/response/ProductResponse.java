package com.target.casestudy.myretail.models.response;

public class ProductResponse {

    private Integer id;

    private String name;

    private PriceResponse current_price;

    public ProductResponse(){

    }

    public ProductResponse(Integer id, String name, PriceResponse current_price) {
        this.id = id;
        this.name = name;
        this.current_price = current_price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PriceResponse getCurrent_price() {
        return current_price;
    }

    public void setCurrent_price(PriceResponse current_price) {
        this.current_price = current_price;
    }
}
