
package com.target.casestudy.myretail.models.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentLabel {

    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("displayed_in_image")
    @Expose
    private DisplayedInImage displayedInImage;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public DisplayedInImage getDisplayedInImage() {
        return displayedInImage;
    }

    public void setDisplayedInImage(DisplayedInImage displayedInImage) {
        this.displayedInImage = displayedInImage;
    }

}
