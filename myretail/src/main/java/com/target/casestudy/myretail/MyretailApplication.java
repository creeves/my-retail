package com.target.casestudy.myretail;

import com.target.casestudy.myretail.repo.PriceRepository;
import com.target.casestudy.myretail.models.dbmodel.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(MongoConfig.class)
public class MyretailApplication implements CommandLineRunner {

	@Autowired
	PriceRepository priceRepository;


	public static void main(String[] args) {
		SpringApplication.run(MyretailApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

		priceRepository.save(new Price(13860428, 2.50, "USD"));
		priceRepository.save(new Price(15117729, 3.50, "USD"));
		priceRepository.save(new Price(16483589, 14.50, "USD"));
	}
}
