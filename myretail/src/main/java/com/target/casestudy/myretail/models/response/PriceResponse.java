package com.target.casestudy.myretail.models.response;

public class PriceResponse {
    private Double value;

    private String currencyCode;

    public PriceResponse(){

    }

    public PriceResponse(Double value, String currencyCode){
        this.value = value;
        this.currencyCode = currencyCode;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
