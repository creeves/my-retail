
package com.target.casestudy.myretail.models.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductJson {

    @SerializedName("product")
    @Expose
    private Product product;

    public ProductJson(Product product){
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
