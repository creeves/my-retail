package com.target.casestudy.myretail.models.dbmodel;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Price {
    @Id
    private Integer id;

    private Double value;

    private String currencyCode;

    public Price(Integer id, Double value, String currencyCode){
        this.setId(id);
        this.setValue(value);
        this.setCurrencyCode(currencyCode);
    }

    public Price() {

    }

    @Override
    public String toString() {
        return "Price{" +
                "id=" + getId().toString() +
                ", value='" + getValue() + '\'' +
                ", currencyCode=" + getCurrencyCode() +
                '}';
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
