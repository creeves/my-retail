package com.target.casestudy.myretail.service;

import com.target.casestudy.myretail.models.response.*;
import com.target.casestudy.myretail.repo.PriceRepository;
import com.target.casestudy.myretail.models.dbmodel.Price;
import com.target.casestudy.myretail.models.json.ProductJson;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.*;
import java.net.*;
import java.lang.reflect.Type;

@Service
public class ProductService {
    PriceRepository priceRepository;

    @Autowired
    public ProductService(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    public ProductResponse retrieveProductResponse(Integer id) {
        ProductResponse productResponse = new ProductResponse();

        ProductJson productMetadata;
        try {
            productMetadata = retrieveProductInformation(id);
        } catch (IOException e) {
            return productResponse;
        }

        String productName = productMetadata.getProduct().getItem().getProductDescription().getTitle();
        PriceResponse productPriceInfo = retrievePrice(id);
        productResponse = new ProductResponse(id, productName, productPriceInfo);
        return productResponse;
    }

    private PriceResponse retrievePrice(Integer id){
        Price price = priceRepository.findById(id).orElse(null);
        PriceResponse priceResponse = new PriceResponse();
        if (price != null) {
            priceResponse = new PriceResponse(price.getValue(), price.getCurrencyCode());
        }
        return priceResponse;
    }

    private ProductJson retrieveProductInformation(Integer id) throws IOException {
        String targetUrl= String.format("https://redsky.target.com/v2/pdp/tcin/%s?excludes=taxonomy,price,promotion,bulk_ship," +
                "rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics", id.toString());

        String json = performGetRequest(new URL(targetUrl));
        Gson gson = new Gson();
        Type gm = new TypeToken<ProductJson>() {}.getType();
        ProductJson product = gson.fromJson(json, gm);

        return product;
    }

    private String performGetRequest(URL url) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        return content.toString();
    }
}
