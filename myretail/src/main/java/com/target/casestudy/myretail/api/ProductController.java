package com.target.casestudy.myretail.api;

import com.target.casestudy.myretail.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.target.casestudy.myretail.models.response.ProductResponse;
import java.io.IOException;


@RestController
@RequestMapping(path = "/products/{id}")
public class ProductController {

    ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ProductResponse getProductInformation(@PathVariable(value = "id") int id) throws IOException {
        ProductResponse productInformation = productService.retrieveProductResponse(id);
        return productInformation;
    }


}
