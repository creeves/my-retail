# My Retail
The my retail API aggregates product data from multiple sources and return it as JSON

#Running the application
Java version 1.8 and Maven are both required to run the application

* Option 1 - From the IDE

    * Import dependencies using Maven

    * Run MyretailApplication

    

* Option 2 - From the command line

    * Run the following in the command line

    ```
    mvn install
    ```

    ```
    java -jar target/myretail-0.0.1-SNAPSHOT.jar
    ```

* When the app is running, access the app by navigating to localhost:8080 in your browser

#Run Application Tests
Run MyRetailTestSuite to run all tests found in the test folder

#Run API Tests using Postman
Import Postman json file found in the *postman_resources* folder

Run Application (default port is 8080)

Use Postman API request to test GET /products/13860428
